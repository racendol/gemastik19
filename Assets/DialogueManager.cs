﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

	private Queue<string> sentencesQueue;
	public Text dialogueText;
	public Text nameText;
	public GameObject dialogueBox;

	void Start () {
		sentencesQueue = new Queue<string>();
		dialogueText.text = "";
		nameText.text = "";
		dialogueBox.SetActive(false);
	}

	public void startDialogue(Dialogue dialogue){
		nameText.text = dialogue.name;

		foreach (string sentence in dialogue.sentences)
		{
			sentencesQueue.Enqueue(sentence);
		}

		dialogueBox.SetActive(true);
	}

	public void displayNextSentences(){
		if (sentencesQueue.Count == 0) {
			dialogueText.text = "";
			nameText.text = "";
			FindObjectOfType<PlayerBehaviour>().canMove = true;
			dialogueBox.SetActive(false);
		} else {
			dialogueText.text = sentencesQueue.Dequeue();
		}
	}
}
