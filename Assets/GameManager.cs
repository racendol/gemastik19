﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public Text timeLimitText;
	public Text winText;
	public float timeLimit;
	public bool stopTimer;
	public bool canWin;

	// Use this for initialization
	void Start () {
		timeLimitText.text = "";
		winText.text = "";
		timeLimit = 90.0f; //ini sec
		stopTimer = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (timeLimit <= 0) {
			stopTimer = true;
		}

		if (!stopTimer) {
			int hour = (int)timeLimit/60; //hour jadi min
			int minute = (int)timeLimit % 60; //min jadi sec
			//TODO: complete ms kalo bisa
			// int sec = (int)((timeLimit - minute)*100 % 60); //sec jadi ms

			timeLimitText.text = hour + ":" + minute;

			timeLimit -= Time.deltaTime;
		}
	}

	public void triggerWin(){
		winText.text = "you win xd";
	}

	public void triggerLose(){
		winText.text = "you lose :(";
	}
}
