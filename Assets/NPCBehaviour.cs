﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBehaviour : MonoBehaviour {

	bool canBeInteracted;
	public Dialogue[] dialogues;

	// Use this for initialization
	void Start () {
		canBeInteracted = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (canBeInteracted) {
			if (Input.GetButtonDown("Submit")){
				triggerDialogue();
				canBeInteracted = false;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		canBeInteracted = true;
	}

	void OnTriggerExit2D(Collider2D other){
		canBeInteracted = false;
	}

	public void triggerDialogue(){
		FindObjectOfType<PlayerBehaviour>().canMove = false;
		foreach (Dialogue dialogue in dialogues)
		{
			FindObjectOfType<DialogueManager>().startDialogue(dialogue);
		}

		FindObjectOfType<GameManager>().canWin = true;
	}
}
