﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarangBehaviour : MonoBehaviour {
	
	public bool canBeMoved;
	public bool isEquipped;

	// Use this for initialization
	void Start () {
		canBeMoved = false;
		isEquipped = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Submit") && canBeMoved == true){
			canBeMoved = false;
			isEquipped = true;
			FindObjectOfType<PlayerBehaviour>().haveBarang = true;
		} else if(Input.GetButtonDown("Submit") && isEquipped){
			canBeMoved = true;
			isEquipped = false;
			FindObjectOfType<PlayerBehaviour>().haveBarang = false;
		}

		if (isEquipped){
			Vector2 pos = transform.position;
			// if(Input.GetAxis("Horizontal") == 1){
			// 	pos.x = FindObjectOfType<PlayerBehaviour>().rb.position.x + size;
			// } else if(Input.GetAxis("Horizontal") == -1){
			// 	pos.x = FindObjectOfType<PlayerBehaviour>().rb.position.x - size;
			// } else if(Input.GetAxis("Vertical") == 1){
			// 	pos.y = FindObjectOfType<PlayerBehaviour>().rb.position.y + size;
			// } else if(Input.GetAxis("Vertical") == -1){
			// 	pos.y = FindObjectOfType<PlayerBehaviour>().rb.position.y - size;
			// }

			pos.x += Input.GetAxisRaw("Horizontal")*Time.fixedDeltaTime*10;
			pos.y += Input.GetAxisRaw("Vertical")*Time.fixedDeltaTime*10;
			transform.position = pos;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		canBeMoved = true;
	}

	void OnTriggerExit2D(Collider2D other){
		canBeMoved = false;
	}
}
