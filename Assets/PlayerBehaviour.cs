﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {

	public Rigidbody2D rb;
	public bool canMove;
	public bool haveBarang;
	public Vector2 pos;
	
	// Start is called before the first frame update
    void Start() {
        canMove = true;
    }

	// Update is called once per frame
	void Update () {
		if (canMove)
		//read user input for movement
		{
			pos.x = Input.GetAxisRaw("Horizontal");
			pos.y = Input.GetAxisRaw("Vertical");
			rb.MovePosition(rb.position + pos*Time.fixedDeltaTime*10);
		}
		
	}
}
